# Build a Hello World C program

CC     = gcc
CFLAGS = -g -Wall

TARGET = readElf

all: $(TARGET)

readElf: readElf.c arguments.c processHeader.c
	$(CC) $(CFLAGS) -o $(TARGET) readElf.c arguments.c processHeader.c

clean:
	rm $(TARGET)
	rm *.o

test:
	./$(TARGET) -h $(TARGET)

