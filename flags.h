///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05 - ReadElf
///
/// @file flags.h
/// @version 1.0
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 05 - ReadElf- EE 491F - Spr 2021
/// @date   21_002_2021
///////////////////////////////////////////////////////////////////////////////
#ifndef FLAGS_H
#define FLAGS_H

#include <stdbool.h>


/*
 * Flags structure maintains what options have been enabled if any at all.
 */
struct Flags {
   bool header;
   bool section;
   bool hexDump;
};

#endif
