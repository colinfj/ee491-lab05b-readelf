///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05 - ReadElf
///
/// @file definitions.h
/// @version 1.0
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 05 - ReadElf - EE 491F - Spr 2021
/// @date   21_002_2021
///////////////////////////////////////////////////////////////////////////////
#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#define PROGRAM "ReadElf"
#define VERSION "1.0"
#define NO_ARG_ERROR 1
#define FILE_ERROR 2
#define ILLEGAL_ARG 3

#endif
