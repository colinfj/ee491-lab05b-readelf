///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
///// EE 491 - Software Reverse Engineering
///// Lab 05 - Read Elf
/////
///// @file processHeader.c
///// @version 1.0
/////
///// @author Colin Jackson <colinfj@hawaii.edu>
///// @brief  Lab 05 - ReadElf - EE 491F - Spr 2021
///// @date   21_002_2021
/////////////////////////////////////////////////////////////////////////////////

#include "processHeader.h"

void process(FILE *filePtr, struct Flags flags) {
   unsigned char magic[EI_NIDENT];

   fread(magic, 1, 16, filePtr);

   if( !checkIfElf(magic) ) {
      exit(EXIT_FAILURE);
   }

   fseek(filePtr, 0, SEEK_SET);

   if( is64(magic[4])) {
      struct Header64 elf64Header;

      fread(&elf64Header, 1, sizeof(elf64Header), filePtr);

      if(flags.header) {
         printHeader64(elf64Header);
      }

      if(flags.section) {
         printSection64(elf64Header, filePtr);
      }

      //if(flags.hexDump) {
       //  printHex64(elf64Header);
      //}
   } else {
      struct Header32 elf32Header;
      fread(&elf32Header, 1, sizeof(elf32Header), filePtr);
      if(flags.header) {
         printHeader32(elf32Header);
      }
      
      //if(flags.section) {
       //  printSection32(elf32Header);
      //}

      //if(flags.hexDump) {
       //  printHex32(elf32Header);
      //}
   }


   
}

bool checkIfElf(unsigned char magic[]) {
   if(magic[0] == 0x7f &&
      magic[1] == 'E' &&
      magic[2] == 'L' &&
      magic[3] == 'F')
   {
      printf("Elf Header:\n");
      return true;
   } else {
      printf("Not a ELF File\n");
      return false;
   }

}

bool is64(unsigned char type) {
   if(type == 2)
      return true;
   else
      return false;
}

void printHeader64(struct Header64 elfHeader) {
   printf("Magic:\t");
   for(int i = 0; i < EI_NIDENT; i++) {
      printf("%02X ", elfHeader.e_ident[i]);
   }
   printf("\n  Class:                             ELF%s\n", classType(elfHeader.e_ident[4]));
   printf("  Data:                              2's complement, %s\n", data(elfHeader.e_ident[5]));
   printf("  Version:                           %x (current)\n", elfHeader.e_version);
   printf("  OS/ABI:                            %s\n", abi(elfHeader.e_ident[7]));
   printf("  ABI Version:                       %x\n", elfHeader.e_ident[8]);
   printf("  Type:                              %s\n", type(elfHeader.e_type));
   printf("  Machine:                           %s\n", machine(elfHeader.e_machine));
   printf("  Version:                           0x%X\n", elfHeader.e_version);
   printf("  Entry point address:               0x%lx\n", elfHeader.e_entry);
   printf("  Start of Program headers:          %d (bytes into file)\n", (int) elfHeader.e_phoff);
   printf("  Start of section headers:          %d (bytes into file)\n", (int) elfHeader.e_shoff);
   printf("  Flags:                             0x%x\n", elfHeader.e_flags);
   printf("  Size of this header:               %d (bytes)\n", (int) elfHeader.e_ehsize);
   printf("  Size of program headers:           %d (bytes)\n", (int) elfHeader.e_phentsize);
   printf("  Number of program headers:         %d\n", (int) elfHeader.e_phnum);
   printf("  Size of section headers:           %d (bytes)\n", (int) elfHeader.e_shentsize);
   printf("  Number of section headers:         %d\n", (int) elfHeader.e_shnum);
   printf("  Section header string table index: %d\n", (int) elfHeader.e_shstrndx);
}

void printHeader32(struct Header32 elfHeader) {
   printf("Magic:\t");
   for(int i = 0; i < EI_NIDENT; i++) {
      printf("%02X ", elfHeader.e_ident[i]);
   }
   printf("\n  Class:                             ELF%s\n", classType(elfHeader.e_ident[4]));
   printf("  Data:                              2's complement, %s\n", data(elfHeader.e_ident[5]));
   printf("  Version:                           %x (current)\n", elfHeader.e_version);
   printf("  OS/ABI:                            %s\n", abi(elfHeader.e_ident[7]));
   printf("  ABI Version:                       %x\n", elfHeader.e_ident[8]);
   printf("  Type:                              %s\n", type(elfHeader.e_type));
   printf("  Machine:                           %s\n", machine(elfHeader.e_machine));
   printf("  Version:                           0x%X\n", elfHeader.e_version);
   printf("  Entry point address:               0x%x\n", (uint32_t) elfHeader.e_entry);
   printf("  Start of Program headers:          %d (bytes into file)\n", (int) elfHeader.e_phoff);
   printf("  Start of section headers:          %d (bytes into file)\n", (int) elfHeader.e_shoff);
   printf("  Flags:                             0x%x\n", elfHeader.e_flags);
   printf("  Size of this header:               %d (bytes)\n", (int) elfHeader.e_ehsize);
   printf("  Size of program headers:           %d (bytes)\n", (int) elfHeader.e_phentsize);
   printf("  Number of program headers:         %d\n", (int) elfHeader.e_phnum);
   printf("  Size of section headers:           %d (bytes)\n", (int) elfHeader.e_shentsize);
   printf("  Number of section headers:         %d\n", (int) elfHeader.e_shnum);
   printf("  Section header string table index: %d\n", (int) elfHeader.e_shstrndx);
}

void printSection64(struct Header64 elfHeader, FILE *filePtr) {
   struct Section64 section64;
   char name[128];
   
   fseek(filePtr, elfHeader.e_shoff, SEEK_SET);

   printf("Section Headers:\n");
   printf("       Name\n");
   printf("       Type            Address            Offset            Link\n");
   printf("       Size            EntSize            Info              Align\n");
   printf("       Flags\n");

   for(int i = 0; i < elfHeader.e_shnum; i++) {
      fseek(filePtr, elfHeader.e_shoff + (i * elfHeader.e_shentsize), SEEK_SET);
      fread(&section64, 1, sizeof(section64), filePtr);
      fseek(filePtr, elfHeader.e_shstrndx + section64.sh_name, SEEK_SET);
      fgets(name, 128, filePtr);

      printf("   [%2d] %s\n", i, name);
      printf("   %16s %016lx %016lx %16x\n", sectionType(section64.sh_type), section64.sh_addr, section64.sh_offset, section64.sh_link);
      printf("   %016lx %016lx %16x %16lx\n", section64.sh_size, section64.sh_entsize, section64.sh_info, section64.sh_addralign);
      printf("   [%016lx]\n", section64.sh_flags);
   }

}

char * sectionType(uint32_t type) {
   switch(type) {
      case 0:
         return "NULL";
      case 1:
         return "PROGBITS";
      case 2:
         return "SYMTAB";
      case 3:
         return "STRTAB";
      case 4:
         return "RELA";
      case 5:
         return "HASH";
      case 6:
         return "DYNAMIC";
      case 7:
         return "NOTE";
      case 8:
         return "NOBITS";
      case 9:
         return "REL";
      case 10:
         return "SHLIB";
      case 11:
         return "DYNSYM";
      case 0x70000000:
         return "LOPROC";
      case 0x7fffffff:
         return "HIPROC";
      case 0x80000000:
         return "LOUSER";
      case 0xffffffff:
         return "HIUSER";
      default:
         return "UNKNOWN";
   }
}


char * classType(unsigned char type) {
   switch(type) {
      case 1:
         return "32";
      case 2:
         return "64";
      default:
         return "Invalid Class";
   }
}

char * data(unsigned char data) {
   switch(data) {
      case 1:
         return "little endian";
      case 2:
         return "big endian";
      default:
         return "invalid data encoding";
   }
}

char * abi(unsigned char abi) {
   switch(abi) {
      case 0: 
      case 1:
         return "UNIX - System V";
      case 2:
         return "HP-UX";
      case 3:
         return "NetBSD";
      case 4:
         return "Linux";
      case 5:
         return "Solaris";
      case 6:
         return "IRIX";
      case 7: 
         return "FreeBSD";
      case 8:
         return "TRU64 UNIX";
      case 9:
         return "ARM architecture";
      case 10:
         return "Stand-alone (embedded)";
      default:
         return "Unkown";
   }
}

char * type(uint16_t type) {
   switch(type) {
      case 0:
         return "NONE (No file type)";
      case 1:
         return "REL (Relocatable file)";
      case 2:
         return "EXEC (Executable file)";
      case 3:
         return "DYN (Shared object file)";
      case 4:
         return "CORE (Core file)";
      case 0xff00:
         return "LOPROC (Processor-specific)";
      case 0xffff:
         return "HIPROC (PROCESSOR-specific)";
      default:
         return "UNKOWN";
   }
}

char * machine(uint16_t machine) {
   switch(machine) {
      case 0:
         return "An unknown machine";
      case 1:
         return "AT&T WE 32100";
      case 2:
         return "Sun Microsystems SPARC";
      case 3:
         return "Intel 80386";
      case 4:
         return "Motorola 68000";
      case 5:
         return "Motorola 88000";
      case 6:
         return "Intel 80860";
      case 7:
         return "MIPS RS3000 (big-endian only)";
      case 8:
         return "HP/PA";
      case 9:
         return "SPARC with enhanced instruction set";
      case 0x3e:
         return "AMD x86-64";
      default:
         return "Unrecognized";
   }
}

