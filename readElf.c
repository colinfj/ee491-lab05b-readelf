///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
///// EE 491 - Software Reverse Engineering
///// Lab 05 - Read Elf
/////
///// @file readElf.c
///// @version 1.0
/////
///// @author Colin Jackson <colinfj@hawaii.edu>
///// @brief  Lab 05 - ReadElf - EE 491F - Spr 2021
///// @date   21_002_2021
/////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "arguments.h"
#include "flags.h"
#include "definitions.h"
#include "processHeader.h"

void initializeFlags(struct Flags *flags);

int main(int argc, char * argv[]) {
   struct Flags flags;
   int fileIndex = 1;

   if(argc <=2) {
      fprintf(stderr, "Too few arguments: Error Code [%d]\n", NO_ARG_ERROR);
      exit(EXIT_FAILURE);
   }

   initializeFlags(&flags);

   if(enableOptions(argc, argv, &flags) == ILLEGAL_ARG) {
         fprintf(stderr, "Invalid Argument given: Error Code [%d]\n", ILLEGAL_ARG);
         exit(EXIT_FAILURE);
   }

   while(fileIndex < argc && argv[fileIndex][0] == '-') {
      fileIndex++;
   }

   FILE *filePtr = fopen(argv[fileIndex], "rb");
   
   if(filePtr == NULL) {
      fprintf(stderr, "Unable to open file\n");
   }

   process(filePtr, flags);


   fclose(filePtr);
   exit(EXIT_SUCCESS);
}


/*
 *
 *
 */
void initializeFlags(struct Flags *flags) {
   flags->header = false;
   flags->section = false;
   flags->hexDump = false;
}
