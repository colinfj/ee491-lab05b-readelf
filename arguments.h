///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05 - ReadElf
///
/// @file arguments.h
/// @version 2.1
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 05 - ReadElf - EE 491F - Spr 2021
/// @date   19_001_2021
///////////////////////////////////////////////////////////////////////////////
#ifndef ARGUMENTS_H
#define ARGUMENTS_H

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include "definitions.h"
#include "flags.h"


int enableOptions(int argc, char *argv[], struct Flags *flags);

#endif
