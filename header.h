///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05 - ReadElf
///
/// @file header.h
/// @version 2.1
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 05 - ReadElf - EE 491F - Spr 2021
/// @date   24_002_2021
///////////////////////////////////////////////////////////////////////////////

#ifndef HEADER_H
#define HEADER_H

#include <stdio.h>
#include <stdint.h>

#define EI_NIDENT 16

typedef struct Header64 {
   unsigned char e_ident[EI_NIDENT];
   uint16_t e_type;
   uint16_t e_machine;
   uint32_t e_version;
   uint64_t e_entry;
   uint64_t e_phoff;
   uint64_t e_shoff;
   uint32_t e_flags;
   uint16_t e_ehsize;
   uint16_t e_phentsize;
   uint16_t e_phnum;
   uint16_t e_shentsize;
   uint16_t e_shnum;
   uint16_t e_shstrndx;
} Header64;

typedef struct Header32 {
   unsigned char e_ident[EI_NIDENT];
   uint16_t e_type;
   uint16_t e_machine;
   uint32_t e_version;
   uint32_t e_entry;
   uint32_t e_phoff;
   uint32_t e_shoff;
   uint32_t e_flags;
   uint16_t e_ehsize;
   uint16_t e_phentsize;
   uint16_t e_phnum;
   uint16_t e_shentsize;
   uint16_t e_shnum;
   uint16_t e_shstrndx;
} Header32;

typedef struct Section64 {
   uint32_t sh_name;
   uint32_t sh_type;
   uint64_t sh_flags;
   uint64_t sh_addr;
   uint64_t sh_offset;
   uint64_t sh_size;
   uint32_t sh_link;
   uint32_t sh_info;
   uint64_t sh_addralign;
   uint64_t sh_entsize;
} Section64;


#endif
