
///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05 - ReadElf
///
/// @file arguments.c
/// @version 1.0
/// @see https://linux.die.net/man/3/getopt for documentation on getopt_long() uses
///
/// @author Colin Jackson <colinfj@hawaii.edu>
/// @brief  Lab 05 - ReadElf -- EE 491F - Spr 2021
/// @date   21_002_2021
///////////////////////////////////////////////////////////////////////////////

#include "arguments.h"
#include "definitions.h"

static int opt = 0;

/*
 *structure containing all valid options for ReadElf program
 */

static struct option long_options[] = {
   {"header", optional_argument, 0, 'h'},
   {0, 0, 0, 0}
};



/*
 * Function: enableOptions
 *
 * @Param: argc - number of arguments from command line
 *         *argv[] - pointer array to arguments from command line
 *         *flags - pointer to flag struct
 *
 * @return 0 - continued running of program with enabled flags
 */
int enableOptions(int argc, char *argv[], struct Flags *flags)
{
   while( (opt = getopt_long(argc, argv, ":htx:", long_options, NULL)) != -1)
   {
      switch(opt)
      {
         case 'h':
            flags->header = true;
            break;
         case 't':
            flags->section = true;
            break;
         case 'x':
            flags->hexDump = true;
            break;
         default:
            return ILLEGAL_ARG;
      }
   }

   return 0;
}

