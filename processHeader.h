///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
///// EE 491 - Software Reverse Engineering
///// Lab 05 - Read Elf
/////
///// @file processHeader.h
///// @version 1.0
/////
///// @author Colin Jackson <colinfj@hawaii.edu>
///// @brief  Lab 05 - ReadElf - EE 491F - Spr 2021
///// @date   21_002_2021
/////////////////////////////////////////////////////////////////////////////////

#ifndef PROCESSHEADER_H
#define PROCESSHEADER_H

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "header.h"
#include "flags.h"

void process(FILE *filePtr, struct Flags flags);
bool checkIfElf(unsigned char magic[]);
bool is64(unsigned char type);
void printHeader64(struct Header64 elfHeader);
void printHeader32(struct Header32 elfHeader);
void printSection64(struct Header64 elfHeader, FILE * filePtr);
void printSection32(struct Header32 elfHeader);
void printHex64(struct Header64 elfHeader);
void printHex32(struct Header32 elfHeader);
char * classType(unsigned char type);
char * data(unsigned char type);
char * abi(unsigned char abi);
char * type(uint16_t type);
char * machine(uint16_t machine);
char * sectionType(uint32_t type);

#endif

